# Dan's kata tests

This is a solution which I use to hold the various projects that I attempt from TDD Katas found online.

It is intended to include only my first attempt at each kata - repeated attempts will be held in a separate project.

Feel free to comment in the issues!

Feedback is welcome (as long as it is not about the amount of coffee that I make for my co-workers).

- [Roman numerals](/RomanNumeralsKata) - kata source: https://github.com/TDD-Katas/roman-numerals
- [String calculator](/StringCalculatorKata) - kata source: http://osherove.com/tdd-kata-1/
- [LCD Digits](/LcdDigits) - kata source: https://github.com/garora/TDD-Katas/blob/master/KatasReadme.md


